# KEYPR Weather App

App architecture is based on Rx Clean Architecture and uses MVVM pattern for presentation layer.

You will need an [**AppId**](https://openweathermap.org/appid) to work with OpenWeather API.

Development
-----------------

Here are some useful **Gradle** useful commands for regular development:

 * `./gradlew clean build` - Builds the entire example and execute unit and integration tests plus lint check.
 * `./gradlew installDebug` - Installs the debug apk on the current connected device.
 * `./gradlew test` - Runs all Unit tests.
 * `./gradlew ktlint` - Runs [ktlint](https://github.com/shyiko/ktlint) to check code style.
 * `./gradlew detekt` - Runs [detekt](https://github.com/arturbosch/detekt) (a static code analysis tool for the _Kotlin_) on the app code.
 * `./gradlew detektAndKtlintCheck` - Runs both **detekt** and **ktlint** checks
 * `./gradlew performCheckAndQuickBuild`  - Runs Unit tests, code checks and build DEBUG app variant (For that reason it builds fast).
  * `./gradlew performCheckAndFullBuild` - Runs Unit tests, code cheks and build full app with all variants.

Code style
-----------
Please follow [ktlint](https://github.com/shyiko/ktlint) documentation for a full list of code style guidelines.

Developed by [amatkivskiy](https://github.com/amatkivskiy/)