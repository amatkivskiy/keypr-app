package com.amatkivksiy.keypr.app

class Constants {
    companion object {
        const val WORK_SCHEDULER_TAG = "work_scheduler"
        const val CALLBACK_SCHEDULER_TAG = "callback_scheduler"

        const val OPEN_WEATHER_HOST = "https://api.openweathermap.org"
        const val APP_ID = "a1d1dc41d71e2b1c1d329e64770bf088"

        const val NETWORK_AVAILABILITY_CHECK_INTERVAL_SECONDS = 3L

        val DEFAULT_CITIES_LIST = listOf(City.Kyiv, City.Toronto, City.London)
    }

    enum class City(val id: String) {
        Kyiv("703448"),
        Toronto("6167865"),
        London("2643743")
    }
}
