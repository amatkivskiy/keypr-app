package com.amatkivksiy.keypr.app.data.network

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface OpenWeatherApi {
    @GET("/data/2.5/group?units=metric")
    fun fetchCityWeather(@Query("id") ids: String, @Query("appid") appId: String):
        Observable<CityWeatherContainer>
}
