package com.amatkivksiy.keypr.app.domain.model

sealed class Error {
    class GeneralError(val exception: Throwable) : Error() {
        override fun toString(): String {
            return "GeneralError(exception=$exception)"
        }
    }
}
