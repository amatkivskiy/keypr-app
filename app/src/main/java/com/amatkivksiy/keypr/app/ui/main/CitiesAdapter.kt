package com.amatkivksiy.keypr.app.ui.main

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.amatkivksiy.keypr.app.R
import com.amatkivksiy.keypr.app.data.viewmodel.WeatherInfo
import com.amatkivksiy.keypr.app.extension.inflate
import com.amatkivksiy.keypr.app.extension.loadImageFromUrl
import kotlinx.android.synthetic.main.item_city_weather.view.*
import kotlin.properties.Delegates

class CitiesAdapter : RecyclerView.Adapter<CitiesAdapter.CityViewHolder>() {
    internal var items: List<WeatherInfo> by Delegates.observable(emptyList()) { _, _, _ ->
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        CityViewHolder(parent.inflate(R.layout.item_city_weather))

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: CityViewHolder, position: Int) {
        holder.bind(items[position])
    }

    class CityViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(weatherInfo: WeatherInfo) {
            itemView.imageViewWeatherIcon.loadImageFromUrl(weatherInfo.iconUrl)
            itemView.textViewLocationName.text = weatherInfo.locationFullName
            itemView.textViewWeatherConditionDescription.text = weatherInfo.weatherCondition
            itemView.textViewTemperature.text = weatherInfo.temperature
        }
    }
}
