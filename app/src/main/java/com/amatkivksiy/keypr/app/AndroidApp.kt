package com.amatkivksiy.keypr.app

import android.app.Application
import com.amatkivksiy.keypr.app.data.di.androidPlatformModule
import com.amatkivksiy.keypr.app.data.di.generalAppModule
import com.amatkivksiy.keypr.app.data.di.useCaseAndViewModelModule
import net.danlew.android.joda.JodaTimeAndroid
import org.koin.android.ext.android.startKoin
import org.koin.android.logger.AndroidLogger
import timber.log.Timber

class AndroidApp : Application() {
    override fun onCreate() {
        super.onCreate()

        JodaTimeAndroid.init(this)

        startKoin(
            this,
            listOf(
                androidPlatformModule(this),
                generalAppModule(Constants.OPEN_WEATHER_HOST, BuildConfig.DEBUG),
                useCaseAndViewModelModule()
            ),
            logger = AndroidLogger(showDebug = BuildConfig.DEBUG)
        )

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        RefreshWeatherWorker.scheduleWorkRequest()
    }
}
