package com.amatkivksiy.keypr.app.domain

import com.amatkivksiy.keypr.app.Constants
import com.amatkivksiy.keypr.app.data.repository.LocalWeatherRepository
import com.amatkivksiy.keypr.app.data.repository.RemoteWeatherRepository
import com.amatkivksiy.keypr.app.domain.model.CityWeather
import com.amatkivksiy.keypr.app.domain.model.Error
import com.amatkivksiy.keypr.app.extension.toResult
import com.amatkivksiy.keypr.app.functional.Result
import io.reactivex.Observable
import io.reactivex.schedulers.Timed
import java.util.concurrent.TimeUnit

interface WeatherDataSource {
    fun getWeatherForDefaultCities(): Observable<Result<Timed<List<CityWeather>>, Error>>

    class DefaultWeatherDataSource(
        private val localWeatherRepository: LocalWeatherRepository,
        private val remoteWeatherRepository: RemoteWeatherRepository
    ) : WeatherDataSource {
        override fun getWeatherForDefaultCities(): Observable<Result<Timed<List<CityWeather>>, Error>> {
            return remoteWeatherRepository.getWeatherForCities(Constants.DEFAULT_CITIES_LIST)
                .timestamp(TimeUnit.SECONDS)
                .doOnNext {
                    localWeatherRepository.cacheWeatherForCities(it)
                }.onErrorResumeNext(Observable.fromCallable {
                    localWeatherRepository.getCachedWeatherForCities()
                })
                .toResult()
        }
    }
}
