package com.amatkivksiy.keypr.app.domain.usecase

import com.amatkivksiy.keypr.app.domain.WeatherDataSource
import com.amatkivksiy.keypr.app.domain.model.CityWeather
import com.amatkivksiy.keypr.app.domain.model.Error
import com.amatkivksiy.keypr.app.functional.Result
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.schedulers.Timed

class GetWeatherForDefaultCitiesUseCase(
    private val weatherDataSource: WeatherDataSource,
    workScheduler: Scheduler,
    callbackScheduler: Scheduler
) : RxUseCase<Timed<List<CityWeather>>, RxUseCase.None>(workScheduler, callbackScheduler) {
    override fun raw(params: None): Observable<Result<Timed<List<CityWeather>>, Error>> {
        return weatherDataSource.getWeatherForDefaultCities()
    }
}
