package com.amatkivksiy.keypr.app.data.repository

import android.content.SharedPreferences

class PreferenceStorage(private val preferences: SharedPreferences) {
    fun storeCitiesWeather(citiesWeatherJson: String) {
        preferences.edit().apply {
            putString(PREF_CITIES_WEATHER_KEY, citiesWeatherJson)

            apply()
        }
    }

    fun getStoredCitiesWeather(): String? {
        return preferences.getString(PREF_CITIES_WEATHER_KEY, null)
    }

    companion object {
        const val PREF_CITIES_WEATHER_KEY = "cities_weather"
    }
}
