package com.amatkivksiy.keypr.app.data.network

import com.amatkivksiy.keypr.app.domain.model.CityWeather
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import java.lang.reflect.Type

class CityWeatherJsonDeserializer : JsonDeserializer<CityWeather> {
    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): CityWeather {
        if (json == null) {
            throw IllegalArgumentException("Received null city weather.")
        }

        val cityWeatherObject = json.asJsonObject
        val name = cityWeatherObject.get("name").asString
        val country = cityWeatherObject.getAsJsonObject("sys").get("country").asString

        // Yeah, from the OpeWeather docs the most relevant weather is located at the beginning of the array
        val weatherDescription =
            cityWeatherObject.getAsJsonArray("weather").get(0).asJsonObject.get("description").asString
        val weatherIcon = cityWeatherObject.getAsJsonArray("weather").get(0).asJsonObject.get("icon").asString

        val temperature = cityWeatherObject.getAsJsonObject("main").get("temp").asString

        return CityWeather(name, country, weatherDescription, weatherIcon, temperature)
    }
}
