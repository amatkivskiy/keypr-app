package com.amatkivksiy.keypr.app.data.repository

import com.amatkivksiy.keypr.app.Constants
import com.amatkivksiy.keypr.app.data.network.OpenWeatherApi
import com.amatkivksiy.keypr.app.domain.model.CityWeather
import io.reactivex.Observable

interface RemoteWeatherRepository {
    fun getWeatherForCities(cities: List<Constants.City>): Observable<List<CityWeather>>

    class NetworkWeatherRepository(private val openWeatherApi: OpenWeatherApi) : RemoteWeatherRepository {
        override fun getWeatherForCities(cities: List<Constants.City>): Observable<List<CityWeather>> {
            val cityIds = cities.joinToString(separator = ",") { it.id }
            return openWeatherApi.fetchCityWeather(cityIds, Constants.APP_ID).map {
                it.list
            }
        }
    }
}
