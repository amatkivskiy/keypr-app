package com.amatkivksiy.keypr.app.data.viewmodel

import androidx.lifecycle.MutableLiveData
import com.amatkivksiy.keypr.app.Constants.Companion.NETWORK_AVAILABILITY_CHECK_INTERVAL_SECONDS
import com.amatkivksiy.keypr.app.domain.model.CityWeather
import com.amatkivksiy.keypr.app.domain.usecase.GetWeatherForDefaultCitiesUseCase
import com.amatkivksiy.keypr.app.domain.usecase.RxUseCase
import io.reactivex.Observable
import io.reactivex.schedulers.Timed
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import java.util.concurrent.TimeUnit

class MainViewModel(private val getWeatherForDefaultCitiesUseCase: GetWeatherForDefaultCitiesUseCase) :
    RxBaseViewModel() {
    var weatherLiveData = MutableLiveData<List<WeatherInfo>>()
    var offlineMessageLiveData = MutableLiveData<ConsumableEvent<Message>>()

    private var latestDataShowed: Timed<List<CityWeather>>? = null

    fun loadWeather() {
        offlineMessageLiveData.value = ConsumableEvent(Message.Hide)

        addDisposable {
            getWeatherForDefaultCitiesUseCase.call(RxUseCase.None())
                .subscribe {
                    it.fold({ data ->
                        latestDataShowed = data

                        weatherLiveData.value = data.value().map(::mapWeatherInfo)
                    }, { error ->
                        handleFailure(error)
                    })
                }
        }
    }

    fun handleConnectionObservable(observable: Observable<Boolean>) {
        addDisposable {
            Observable.interval(
                NETWORK_AVAILABILITY_CHECK_INTERVAL_SECONDS,
                NETWORK_AVAILABILITY_CHECK_INTERVAL_SECONDS,
                TimeUnit.SECONDS
            )
                .flatMap { observable }
                // Notify only on value changes
                .distinctUntilChanged()
                // Ignore initial value
                .skip(1)
                .subscribe { isOnline ->
                    if (isOnline)
                        latestDataShowed?.let {
                            val time = DateTimeFormat.forPattern("HH:mm")
                                .print(DateTime(TimeUnit.SECONDS.toMillis(it.time())))

                            offlineMessageLiveData.postValue(ConsumableEvent(Message.Show(time)))
                        }
                }
        }
    }

    private fun mapWeatherInfo(cityWeather: CityWeather) =
        WeatherInfo(
            iconUrl = "https://openweathermap.org/img/w/${cityWeather.weatherIconId}.png",
            locationFullName = "${cityWeather.name}, ${cityWeather.country}",
            weatherCondition = cityWeather.weatherDescription,
            temperature = "${cityWeather.temperature}°"
        )

    sealed class Message {
        class Show(val time: String) : Message()
        object Hide : Message()
    }
}
