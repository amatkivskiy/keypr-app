package com.amatkivksiy.keypr.app.data.di

import android.content.Context
import android.preference.PreferenceManager
import com.amatkivksiy.keypr.app.Constants
import com.amatkivksiy.keypr.app.data.network.createApi
import com.amatkivksiy.keypr.app.data.repository.LocalWeatherRepository
import com.amatkivksiy.keypr.app.data.repository.LocalWeatherRepository.PreferencesLocalWeatherRepository
import com.amatkivksiy.keypr.app.data.repository.PreferenceStorage
import com.amatkivksiy.keypr.app.data.repository.RemoteWeatherRepository
import com.amatkivksiy.keypr.app.data.repository.RemoteWeatherRepository.NetworkWeatherRepository
import com.amatkivksiy.keypr.app.data.viewmodel.MainViewModel
import com.amatkivksiy.keypr.app.domain.WeatherDataSource
import com.amatkivksiy.keypr.app.domain.WeatherDataSource.DefaultWeatherDataSource
import com.amatkivksiy.keypr.app.domain.usecase.GetWeatherForDefaultCitiesUseCase
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

fun androidPlatformModule(context: Context) = module {
    single { PreferenceStorage(PreferenceManager.getDefaultSharedPreferences(context)) }
    single(Constants.WORK_SCHEDULER_TAG) { Schedulers.io() }
    single(Constants.CALLBACK_SCHEDULER_TAG) { AndroidSchedulers.mainThread() }
}

fun generalAppModule(baseUrl: String, networkLogging: Boolean) = module {
    single {
        createApi(
            debug = networkLogging,
            baseUrl = baseUrl
        )
    }

    single { PreferencesLocalWeatherRepository(Gson(), get()) as LocalWeatherRepository }
    single { NetworkWeatherRepository(get()) as RemoteWeatherRepository }
    single { DefaultWeatherDataSource(get(), get()) as WeatherDataSource }
}

fun useCaseAndViewModelModule() = module {
    factory {
        GetWeatherForDefaultCitiesUseCase(
            get(),
            get(Constants.WORK_SCHEDULER_TAG),
            get(Constants.CALLBACK_SCHEDULER_TAG)
        )
    }
    viewModel { MainViewModel(get()) }
}
