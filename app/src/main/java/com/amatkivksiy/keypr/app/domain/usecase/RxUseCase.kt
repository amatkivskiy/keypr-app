package com.amatkivksiy.keypr.app.domain.usecase

import androidx.annotation.VisibleForTesting
import com.amatkivksiy.keypr.app.domain.model.Error
import com.amatkivksiy.keypr.app.functional.Result
import io.reactivex.Observable
import io.reactivex.Scheduler

abstract class RxUseCase<Output : Any, in Params : Any>(
    private val workScheduler: Scheduler,
    private val callbackScheduler: Scheduler
) {
    @VisibleForTesting
    abstract fun raw(params: Params): Observable<Result<Output, Error>>

    fun call(params: Params): Observable<Result<Output, Error>> {
        return raw(params)
            .observeOn(callbackScheduler)
            .subscribeOn(workScheduler)
    }

    class None
}
