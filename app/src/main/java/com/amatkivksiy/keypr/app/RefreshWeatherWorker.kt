package com.amatkivksiy.keypr.app

import android.annotation.SuppressLint
import android.content.Context
import androidx.work.Constraints
import androidx.work.ListenableWorker
import androidx.work.NetworkType
import androidx.work.PeriodicWorkRequest
import androidx.work.WorkManager
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.amatkivksiy.keypr.app.domain.model.Error
import com.amatkivksiy.keypr.app.domain.usecase.GetWeatherForDefaultCitiesUseCase
import com.amatkivksiy.keypr.app.domain.usecase.RxUseCase
import com.amatkivksiy.keypr.app.functional.Result.Companion.failure
import com.amatkivksiy.keypr.app.functional.Result.Success
import org.koin.standalone.KoinComponent
import org.koin.standalone.get
import timber.log.Timber
import java.util.concurrent.TimeUnit

typealias WorkerResult = ListenableWorker.Result

class RefreshWeatherWorker(context: Context, workerParams: WorkerParameters) : Worker(context, workerParams),
    KoinComponent {
    @SuppressLint("CheckResult")
    override fun doWork(): WorkerResult {
        val getWeatherForDefaultCitiesUseCase = get<GetWeatherForDefaultCitiesUseCase>()

        val result = getWeatherForDefaultCitiesUseCase.call(RxUseCase.None())
            .blockingFirst(
                failure(Error.GeneralError(IllegalStateException("Nothing emitted")))
            )

        return if (result is Success) {
            Timber.d("Successfully fetched weather data. Received: ${result.value}")
            WorkerResult.SUCCESS
        } else {
            Timber.d("Failed to fetch weather data. Error: $result")
            WorkerResult.FAILURE
        }
    }

    companion object {
        private const val TAG = "refresh_weather"

        fun scheduleWorkRequest() {
            WorkManager.getInstance().cancelAllWorkByTag(TAG)

            val constraints = Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build()

            val request = PeriodicWorkRequest.Builder(RefreshWeatherWorker::class.java, 1, TimeUnit.HOURS)
                .addTag(TAG)
                .setConstraints(constraints)
                .build()

            WorkManager.getInstance().enqueue(request)
        }
    }
}
