package com.amatkivksiy.keypr.app.data.viewmodel

data class WeatherInfo(
    val iconUrl: String,
    val locationFullName: String,
    val weatherCondition: String,
    val temperature: String
)
