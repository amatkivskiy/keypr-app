package com.amatkivksiy.keypr.app.domain.model

data class CityWeather(
    val name: String,
    val country: String,
    val weatherDescription: String,
    val weatherIconId: String,
    val temperature: String
)
