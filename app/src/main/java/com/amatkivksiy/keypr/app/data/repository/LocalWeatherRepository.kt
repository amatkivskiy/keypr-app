package com.amatkivksiy.keypr.app.data.repository

import com.amatkivksiy.keypr.app.data.network.fromJson
import com.amatkivksiy.keypr.app.domain.model.CityWeather
import com.google.gson.Gson
import io.reactivex.schedulers.Timed

interface LocalWeatherRepository {
    fun getCachedWeatherForCities(): Timed<List<CityWeather>>
    fun cacheWeatherForCities(weather: Timed<List<CityWeather>>)

    class PreferencesLocalWeatherRepository(private val gson: Gson, private val preferenceStorage: PreferenceStorage) :
        LocalWeatherRepository {
        override fun cacheWeatherForCities(weather: Timed<List<CityWeather>>) {
            val jsonString = gson.toJson(weather)
            preferenceStorage.storeCitiesWeather(jsonString)
        }

        override fun getCachedWeatherForCities(): Timed<List<CityWeather>> {
            val json = preferenceStorage.getStoredCitiesWeather()
            return gson.fromJson(json) ?: throw NothingInCache()
        }
    }

    class NothingInCache : Exception("Local cache is empty")
}
