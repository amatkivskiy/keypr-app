package com.amatkivksiy.keypr.app.data.network

import com.amatkivksiy.keypr.app.domain.model.CityWeather

data class CityWeatherContainer(val list: List<CityWeather>)
