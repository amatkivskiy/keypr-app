package com.amatkivksiy.keypr.app.ui.main

import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import com.amatkivksiy.keypr.app.R
import com.amatkivksiy.keypr.app.data.viewmodel.ConsumableEvent
import com.amatkivksiy.keypr.app.data.viewmodel.MainViewModel
import com.amatkivksiy.keypr.app.data.viewmodel.WeatherInfo
import com.amatkivksiy.keypr.app.domain.model.Error
import com.amatkivksiy.keypr.app.extension.observe
import com.google.android.material.snackbar.Snackbar
import io.reactivex.Observable
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    private val viewModel: MainViewModel by viewModel()
    private val citiesAdapter = CitiesAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initializeView()

        observe(viewModel.weatherLiveData) {
            renderItems(it)
        }
        observe(viewModel.failure) {
            renderError(it)
        }
        observe(viewModel.offlineMessageLiveData) {
            renderOfflineNoticeMessage(it)
        }

        setupNetworkAvailabilityCheck()
    }

    override fun onResume() {
        super.onResume()

        startLoading()
    }

    private fun setupNetworkAvailabilityCheck() {
        viewModel.handleConnectionObservable(Observable.fromCallable {
            val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetworkInfo = connectivityManager.activeNetworkInfo
            activeNetworkInfo != null && activeNetworkInfo.isConnected
        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_refresh -> {
                startLoading()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun startLoading() {
        swipeRefreshLayout.isRefreshing = true

        viewModel.loadWeather()
    }

    private fun initializeView() {
        setTitle(R.string.text_main_screen_title)

        recyclerViewCities.addItemDecoration(
            DividerItemDecoration(
                applicationContext,
                DividerItemDecoration.VERTICAL
            )
        )

        recyclerViewCities.setHasFixedSize(true)
        recyclerViewCities.emptyView = textViewEmptyView
        recyclerViewCities.adapter = citiesAdapter

        swipeRefreshLayout.setColorSchemeResources(
            R.color.colorPrimary,
            android.R.color.holo_green_dark,
            android.R.color.holo_orange_dark,
            android.R.color.holo_blue_dark
        )
        swipeRefreshLayout.setOnRefreshListener {
            startLoading()
        }
    }

    private fun renderOfflineNoticeMessage(event: ConsumableEvent<MainViewModel.Message>) {
        event.handleIfNotConsumed {
            when (it) {
                is MainViewModel.Message.Show -> showOfflineNoticeViewWithValue(it.time)
                is MainViewModel.Message.Hide -> hideOfflineNoticeView()
            }
        }
    }

    private fun renderError(it: Error) {
        swipeRefreshLayout.isRefreshing = false
        if (it is Error.GeneralError) {
            Snackbar.make(rootView, "Sorry, faced an error. Error : ${it.exception.message}", Snackbar.LENGTH_LONG)
                .show()
        }
    }

    private fun renderItems(it: List<WeatherInfo>) {
        swipeRefreshLayout.isRefreshing = false
        citiesAdapter.items = it
    }

    private fun showOfflineNoticeViewWithValue(lastUpdateTime: String) {
        textViewOfflineNotice.text = getString(R.string.text_offline_mode, lastUpdateTime)
        textViewOfflineNotice.visibility = View.VISIBLE
    }

    private fun hideOfflineNoticeView() {
        textViewOfflineNotice.visibility = View.GONE
    }
}
