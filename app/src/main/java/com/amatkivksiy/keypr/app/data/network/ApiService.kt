package com.amatkivksiy.keypr.app.data.network

import com.amatkivksiy.keypr.app.Constants
import com.amatkivksiy.keypr.app.domain.model.CityWeather
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

fun createApi(
    debug: Boolean,
    baseUrl: String = Constants.OPEN_WEATHER_HOST
): OpenWeatherApi {
    val okClient = OkHttpClient.Builder().apply {
        if (debug) {
            val loggingInterceptor = HttpLoggingInterceptor {
                println(it)
            }
                .apply {
                    level = HttpLoggingInterceptor.Level.BASIC
                }
            addInterceptor(loggingInterceptor)
        }
    }.build()

    return Retrofit.Builder()
        .baseUrl(baseUrl)
        .client(okClient)
        .addConverterFactory(GsonConverterFactory.create(createNetworkResponseGson()))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
        .create(OpenWeatherApi::class.java)
}

private fun createNetworkResponseGson(): Gson {
    return GsonBuilder()
        .registerTypeAdapter(CityWeather::class.java, CityWeatherJsonDeserializer())
        .create()
}

inline fun <reified T> Gson.fromJson(json: String?): T = this.fromJson<T>(json, object : TypeToken<T>() {}.type)
