package com.amatkivksiy.keypr.app.testutils

import io.reactivex.Observable

fun <T> Observable<T>.checkAndGet(): T {
    return this.test()
        .assertValueCount(1)
        .assertNoErrors()
        .assertComplete()
        .values()[0]
}
