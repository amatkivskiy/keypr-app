package com.amatkivksiy.keypr.app.data.repository

import android.content.SharedPreferences
import com.amatkivksiy.keypr.app.data.repository.LocalWeatherRepository.PreferencesLocalWeatherRepository
import com.amatkivksiy.keypr.app.data.repository.PreferenceStorage.Companion.PREF_CITIES_WEATHER_KEY
import com.amatkivksiy.keypr.app.domain.model.CityWeather
import com.amatkivksiy.keypr.app.testutils.textFromFile
import com.google.gson.Gson
import io.mockk.Runs
import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.just
import io.mockk.mockk
import io.mockk.slot
import io.reactivex.schedulers.Timed
import org.amshove.kluent.shouldBeEqualTo
import org.amshove.kluent.shouldEqual
import org.junit.Before
import org.junit.Test
import java.util.concurrent.TimeUnit

class PreferencesLocalWeatherRepositoryTest {
    private val editor = mockk<SharedPreferences.Editor>()
    private val preferences = mockk<SharedPreferences>()
    private val preferenceStorage = PreferenceStorage(preferences)
    private val preferencesRepository = PreferencesLocalWeatherRepository(Gson(), preferenceStorage)

    @Test(expected = LocalWeatherRepository.NothingInCache::class)
    fun `test preference repository throws exception if nothing cached`() {
        every { preferences.getString(PREF_CITIES_WEATHER_KEY, null) } answers {
            null
        }

        preferencesRepository.getCachedWeatherForCities()
    }

    @Test
    fun `test preference repository passes data to preference storage correctly`() {
        every { preferences.getString(PREF_CITIES_WEATHER_KEY, null) } answers {
            textFromFile("cached-city-weather.json")
        }

        val result = preferencesRepository.getCachedWeatherForCities()

        result.shouldEqual(testCitiesData())
    }

    @Test
    fun `test preference repository fetches data from preference storage correctly`() {
        val slot = slot<String>()
        every { preferences.edit() } answers { editor }
        every {
            editor.putString(PREF_CITIES_WEATHER_KEY, capture(slot))
        } answers {
            editor
        }
        every { editor.apply() } just Runs

        preferencesRepository.cacheWeatherForCities(testCitiesData())

        slot.captured.shouldBeEqualTo(textFromFile("cached-city-weather.json"))
    }

    @Before
    fun setUp() {
        clearAllMocks()
    }

    private fun testCitiesData() = Timed(
        listOf(
            CityWeather(
                "Kiev",
                "UA",
                "broken clouds",
                "04d",
                "-2.66"
            )
        ), 0, TimeUnit.SECONDS
    )
}
