package com.amatkivksiy.keypr.app.domain.usecase

import com.amatkivksiy.keypr.app.domain.WeatherDataSource
import com.amatkivksiy.keypr.app.domain.model.CityWeather
import com.amatkivksiy.keypr.app.functional.Result
import com.amatkivksiy.keypr.app.testutils.checkAndGet
import com.amatkivksiy.keypr.app.testutils.failIfError
import com.amatkivksiy.keypr.app.testutils.startKoin
import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.mockk
import io.reactivex.Observable
import io.reactivex.schedulers.Timed
import org.amshove.kluent.shouldEqual
import org.junit.Before
import org.junit.Test
import org.koin.dsl.module.module
import org.koin.standalone.inject
import org.koin.test.AutoCloseKoinTest
import java.util.concurrent.TimeUnit

class GetWeatherForDefaultCitiesUseCaseTest : AutoCloseKoinTest() {
    private val weatherDataSource = mockk<WeatherDataSource>()

    private val useCase: GetWeatherForDefaultCitiesUseCase by inject()

    @Test
    fun `use case returns correct data when requested`() {
        val testData = Timed(
            listOf(
                CityWeather(
                    emptyString(),
                    emptyString(),
                    emptyString(),
                    emptyString(),
                    emptyString()
                )
            ), 0, TimeUnit.SECONDS
        )

        every {
            weatherDataSource.getWeatherForDefaultCities()
        } answers {
            Observable.just(Result.success(testData))
        }

        val result = useCase.raw(RxUseCase.None())
            .checkAndGet()

        result.fold({
            it.shouldEqual(testData)
        }, ::failIfError)
    }

    @Before
    fun setUp() {
        clearAllMocks()

        startKoin(overridesModule = module(override = true) {
            single { weatherDataSource }
        })
    }

    private fun emptyString() = ""
}
