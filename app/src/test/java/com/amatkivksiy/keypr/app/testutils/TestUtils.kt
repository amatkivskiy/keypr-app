package com.amatkivksiy.keypr.app.testutils

import com.amatkivksiy.keypr.app.domain.model.Error
import org.junit.Assert.fail

fun failIfError(error: Error) {
    fail("Something went wrong. Error callback should not be triggered here. Error : $error")
}

fun failIfSuccess(value: Any) {
    fail("Something went wrong. Successful callback should not be triggered here. Value : $value")
}

// TODO: remove later
fun doNothingForSuccess(value: Any) {
    // Everything is OK. Just skipp.
}
