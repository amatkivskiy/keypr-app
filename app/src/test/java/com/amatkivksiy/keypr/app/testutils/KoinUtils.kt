package com.amatkivksiy.keypr.app.testutils

import com.amatkivksiy.keypr.app.Constants
import com.amatkivksiy.keypr.app.data.di.generalAppModule
import com.amatkivksiy.keypr.app.data.di.useCaseAndViewModelModule
import io.reactivex.Scheduler
import io.reactivex.schedulers.TestScheduler
import org.koin.core.Koin
import org.koin.dsl.module.Module
import org.koin.dsl.module.module
import org.koin.log.EmptyLogger
import org.koin.standalone.StandAloneContext

fun startKoin(
    overridesModule: Module,
    baseUrl: String = Constants.OPEN_WEATHER_HOST,
    networkLogging: Boolean = false
): Koin {
    val testScheduler = TestScheduler()

    // Gather all required dependencies
    val allModules = listOf(
        generalAppModule(baseUrl, networkLogging),
        module {
            single(Constants.WORK_SCHEDULER_TAG) { testScheduler as Scheduler }
            single(Constants.CALLBACK_SCHEDULER_TAG) { testScheduler as Scheduler }
        },
        useCaseAndViewModelModule(),
        overridesModule
    )

    return StandAloneContext.startKoin(
        list = allModules,
        logger = EmptyLogger()
    )
}
