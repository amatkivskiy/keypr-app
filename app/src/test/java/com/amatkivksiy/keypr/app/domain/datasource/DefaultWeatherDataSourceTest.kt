package com.amatkivksiy.keypr.app.domain.datasource

import com.amatkivksiy.keypr.app.data.repository.LocalWeatherRepository
import com.amatkivksiy.keypr.app.domain.WeatherDataSource
import com.amatkivksiy.keypr.app.domain.model.CityWeather
import com.amatkivksiy.keypr.app.domain.model.Error
import com.amatkivksiy.keypr.app.testutils.checkAndGet
import com.amatkivksiy.keypr.app.testutils.failIfError
import com.amatkivksiy.keypr.app.testutils.failIfSuccess
import com.amatkivksiy.keypr.app.testutils.init
import com.amatkivksiy.keypr.app.testutils.startKoin
import com.amatkivksiy.keypr.app.testutils.successFromFile
import io.mockk.Runs
import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.just
import io.mockk.mockk
import io.mockk.slot
import io.reactivex.schedulers.Timed
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.amshove.kluent.shouldBeInstanceOf
import org.amshove.kluent.shouldEqual
import org.junit.Before
import org.junit.Test
import org.koin.dsl.module.module
import org.koin.standalone.inject
import org.koin.test.AutoCloseKoinTest
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeUnit.SECONDS

class DefaultWeatherDataSourceTest : AutoCloseKoinTest() {
    private val mockWebServer = MockWebServer()
    private val localWeatherRepository = mockk<LocalWeatherRepository>()
    private val dataSource: WeatherDataSource by inject()

    @Test
    fun `test get cities weather returns error if network request fails and cache is empty`() {
        mockWebServer.enqueue(MockResponse().setResponseCode(500))

        every {
            localWeatherRepository.getCachedWeatherForCities()
        } throws (LocalWeatherRepository.NothingInCache())

        val weatherResult =
            dataSource.getWeatherForDefaultCities()
                .checkAndGet()

        weatherResult.fold(::failIfSuccess) {
            it.shouldBeInstanceOf(Error.GeneralError::class)

            (it as Error.GeneralError).exception.shouldBeInstanceOf(LocalWeatherRepository.NothingInCache::class)
        }
    }

    @Test
    fun `test get cities weather returns correct cities list and caches received result`() {
        mockWebServer.enqueue(successFromFile("get-cities-weather-success.json"))

        val slot = slot<Timed<List<CityWeather>>>()
        every {
            localWeatherRepository.cacheWeatherForCities(weather = capture(slot))
        } just Runs

        val weatherResult =
            dataSource.getWeatherForDefaultCities()
                .checkAndGet()

        weatherResult.fold({
            it.shouldEqual(testCitiesData())
        }, ::failIfError)

        slot.captured.shouldEqual(testCitiesData())
    }

    @Test
    fun `test get cities weather returns cached cities if network request fails`() {
        mockWebServer.enqueue(MockResponse().setResponseCode(500))

        every {
            localWeatherRepository.getCachedWeatherForCities()
        } answers {
            testCitiesData()
        }

        val weatherResult =
            dataSource.getWeatherForDefaultCities()
                .checkAndGet()

        weatherResult.fold({
            it.shouldEqual(testCitiesData())
        }, ::failIfError)
    }

    @Before
    fun setUp() {
        clearAllMocks()

        val mockedBaseUrl = mockWebServer.init()
        startKoin(overridesModule = module(override = true) {
            single { localWeatherRepository }
        }, baseUrl = mockedBaseUrl)
    }

    private fun testCitiesData() = Timed(
        listOf(
            CityWeather(
                "Kiev",
                "UA",
                "broken clouds",
                "04d",
                "-2.66"
            ),
            CityWeather(
                "Toronto",
                "CA",
                "light rain",
                "10n",
                "1.5"
            ),
            CityWeather(
                "London",
                "GB",
                "clear sky",
                "01d",
                "7.8"
            )
        ), TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()), SECONDS
    )
}
